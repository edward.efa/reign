import React, { useEffect, useState } from 'react';
import deleteForever from './assets/deleteForEver.svg';
import './App.css';
import { getStories, deleteStory } from './providers/storyProvider';
import { Story } from './interfaces/Story';


function App() {

  const [stories, setStories] = useState<Story[]>([])

  const getStoriesData = async () => {
    const result = await getStories();
    if( result ) {
      setStories(result);
    }
  }

  const deleteStoryClick = async (e: any,  story: Story ) => {
    e.stopPropagation();
    const result = await deleteStory(story._id);
    if ( result ) {
      const newStories = stories.filter(fStory => fStory != story);
      setStories(newStories);
    }
  }

  const openNewTab = ( story: Story ) => {
    const link = story.story_url || story.url;
    window.open(link, "_blank")
  }

  useEffect(() => {
    getStoriesData();
  }, []);



  return (
    <div className="App">
      <header className="App-header">
        <h1>HN Feed</h1>
        <h4>{"We <3 hacker news!"}</h4>
      </header>
      <div className="App-body">
        { stories.map(story => {
          if (story.story_title || story.title ) {
            const date = new Date(story.created_at_i * 1000);
            const link = story.story_url || story.url;
            console.log(story._id)
            return (
              <div
                key={story._id} 
                className={"item-row " + ( link ? ' link' : '')}
                onClick={link ? () => openNewTab(story) : undefined }
              >
                <div>
                  { `${story.story_title || story.title} - ${story.objectID} ` }
                  <span className="author-row">- {`${story.author}`}</span>
                </div>
                <div>
                  { date.toLocaleTimeString("en-US") }
                </div>
                <div>
                  <img 
                    onClick={(e) => deleteStoryClick(e, story)} 
                    src={deleteForever} 
                    className="App-logo" alt="logo" 
                  />
                </div>
              </div>
            );
          }
          return <></>
        })}
      </div>
    </div>
  );
}

export default App;
