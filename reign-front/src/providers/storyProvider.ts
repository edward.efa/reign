import { Story } from "../interfaces/Story";

const url = process.env.REACT_APP_API;

export const getStories = async (): Promise<Story[]>  => {
    const result = await fetch(`${url}/story`, {
        method: 'GET', 
        mode: 'cors',
        credentials: 'same-origin',
        headers: { 'Content-Type': 'application/json' },
    });
    if ( result.status === 200 ) {
        const resultJson = await result.json();
        return resultJson;
    }
    console.log(result)
    return [];
}
export const deleteStory = async (id: string): Promise<Boolean>  => {
    const result = await fetch(`${url}/story/${id}`, {
        method: 'DELETE', 
        mode: 'cors',
        credentials: 'same-origin',
        headers: { 'Content-Type': 'application/json' },
    });
    return result.status === 200;
}
