export interface Story {
    _id: string;
    story_id: number;
    objectID: string;
    title: string;
    story_title: string;
    author: string;
    url: string;
    story_url: string;
    created_at_i: number;
}