import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

import { StoryModule } from './story/story.module';

@Module({
  imports: [
    // MongooseModule.forRoot(process.env.MONGO_DB),
    MongooseModule.forRoot('mongodb://mongoDb/quotes'),
    ScheduleModule.forRoot(),
    StoryModule,
  ],
})
export class AppModule {}
