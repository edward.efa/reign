import * as mongoose from 'mongoose';

export const StorySchema = new mongoose.Schema({
    story_id: Number,
    objectID: String,
    title: String,
    story_title: String,
    author: String,
    url: String,
    story_url: String,
    created_at_i: Number
});

