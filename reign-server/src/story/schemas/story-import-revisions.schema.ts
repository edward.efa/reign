import * as mongoose from 'mongoose';

export const StoryImportRevisionsSchema = new mongoose.Schema({
    created_at_i: Number,
    last_updated: Date,
});

