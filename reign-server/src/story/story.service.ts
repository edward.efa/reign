import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, Timeout, Interval } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { CreateStoryImportRevisionDto } from './dto/create-story-import-revisions.dto';
import { StoryImportRevision } from './interfaces/story-import-revisions.interfaces';
const axios = require('axios');

import { Story } from './interfaces/story.interfaces';



@Injectable()
export class StoryService {

    constructor(
        @InjectModel('Story') private readonly storyModel: Model<Story>, 
        @InjectModel('StoryImportRevisions') readonly storyImportRevisionsModel: Model<StoryImportRevision>) {}

    // @Cron('* 5 * * * *')
    @Interval(3600000) // 60000
    async importStoryFromAlgoliaCron() {
        const date = new Date();
        console.log(`Story update task every hour ${date}`);
        const lastRevision = await this.getLastRevision()
        await this.importStoryFromAlgolia(lastRevision);
    }

    @Timeout(2000)
    async importStoryFromAlgoliaFirstTime() {
        const lastRevision = await this.getLastRevision();
        if ( !lastRevision ) {
            console.log('first execution!')
            await this.importStoryFromAlgolia(null);
        } 
    }


    async getLastRevision(): Promise<StoryImportRevision> {
        const revisions: StoryImportRevision[] = await this.storyImportRevisionsModel
                .find()
                .sort({'created_at_i': 'desc'})
                .limit(1)
                .exec();
        return revisions.length > 0 ? revisions[0] : null;
    }

    async saveRevision(timestamp) {
        const newStoryRevision = new this.storyImportRevisionsModel({
            created_at_i: timestamp, 
            last_updated: Date.now()});
        await newStoryRevision.save();
    }

    async importStoryFromAlgolia(lastRevision: StoryImportRevision) {

        const storyAlgolia = await axios.get(`${process.env.ALGOLIA_API}?query=nodejs`);
        if ( storyAlgolia.status === 200 ) {

            const algoliaItems: CreateStoryImportRevisionDto[] = storyAlgolia.data.hits;
            let newItems = [];
            let maxTime: number;
            for (let i = 0; i < algoliaItems.length; i++) {
                if ( !lastRevision || lastRevision.created_at_i < algoliaItems[i].created_at_i) {
                    newItems.push(algoliaItems[i]);
                    if ( !maxTime || maxTime < algoliaItems[i].created_at_i ) {
                        maxTime = algoliaItems[i].created_at_i;
                    }
                }
            }
            if ( newItems.length > 0 ) {
                await this.storyModel.insertMany(newItems);
                await this.saveRevision(maxTime);
            } else {
                console.log('any new data');
            }
        }
    }

    async getStories(): Promise<Story[]> {
        return await this.storyModel
                .find()
                .sort({'created_at_i': 'desc'})
                .exec();
    }

    async deleteStory( id: string ): Promise<Story> {
        return await this.storyModel.findByIdAndRemove(id);
    }
}
