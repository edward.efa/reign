import { ApiProperty } from '@nestjs/swagger';

export class CreateStoryDto {
    @ApiProperty()
    readonly story_id: number;
    @ApiProperty()
    readonly objectID: string;
    @ApiProperty()
    readonly title: string;
    @ApiProperty()
    readonly story_title: string;
    @ApiProperty()
    readonly author: string;
    @ApiProperty()
    readonly url: string;
    @ApiProperty()
    readonly story_url: string;
    @ApiProperty()
    readonly created_at_i: number;
}
