import { ApiProperty } from '@nestjs/swagger';

export class CreateStoryImportRevisionDto {
    @ApiProperty()
    readonly created_at_i: number;
    readonly last_updated: Date;
}
