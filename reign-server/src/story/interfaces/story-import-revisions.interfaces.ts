

export interface StoryImportRevision {
    created_at_i: number;
    last_updated: Date;
}