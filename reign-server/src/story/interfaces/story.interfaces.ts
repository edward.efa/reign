export interface Story {
    story_id: number;
    objectID: String,
    title: string;
    story_title: string;
    author: string;
    url: string;
    story_url: string;
    created_at_i: number;
}
