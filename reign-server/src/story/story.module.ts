import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StoryImportRevisionsSchema } from './schemas/story-import-revisions.schema';
import { StorySchema } from './schemas/story.schema';
import { StoryController } from './story.controller';
import { StoryService } from './story.service';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Story', schema: StorySchema}]),
    MongooseModule.forFeature([{name: 'StoryImportRevisions', schema: StoryImportRevisionsSchema}])
  ],
  controllers: [StoryController],
  providers: [StoryService]
})
export class StoryModule {}
