import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ApiTags, ApiParam } from '@nestjs/swagger';
import { Story } from './interfaces/story.interfaces';
import { StoryService } from './story.service';


@ApiTags('Story')
@Controller('story')
export class StoryController {

    constructor(private storyService: StoryService) {}

    @Get()
    async getStories(): Promise<Story[]> {
        return await this.storyService.getStories();
    }

    @ApiParam({ name: 'id' })
    @Delete(':id')
    deleteQuote(@Param('id') id): Promise<Story> {
        return this.storyService.deleteStory(id);
    }
}
