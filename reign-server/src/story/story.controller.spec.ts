import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';

import { closeInMongodConnection, rootMongooseTestModule } from '../test-utils/MongoTestModule';

import { StoryService } from './story.service';

import { StorySchema } from './schemas/story.schema';
import { StoryImportRevisionsSchema } from './schemas/story-import-revisions.schema';



describe('StoryService', () => {
  let service: StoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: 'Story', schema: StorySchema }]),
        MongooseModule.forFeature([{name: 'StoryImportRevisions', schema: StoryImportRevisionsSchema}])
      ],
      providers: [StoryService],
    }).compile();

    service = module.get<StoryService>(StoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  

  afterAll(async () => {
    await closeInMongodConnection();
  });
});
